# Beamsplitter Printing and Assembly Instructions

# Overview

In this repository you will find all the models, parts, and instructions to print and assemble your own Multispectrum Beamsplitter system. If you are reading these instructions somewhere other than on GitLab, please visit the official repository for the latest updates: [https://gitlab.com/multispectrum-beamsplitter/](https://gitlab.com/multispectrum-beamsplitter/)

# Licensing

This project is © 2023 by [Drew Fulton](https://www.drewfulton.com) licensed under [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/)

# Bill of Materials

Below is a complete list of required materials to build out a complete system. Note, some external links may be affiliate links where we may earn a small commission on your purchase at no cost to you.

## 3D Printer and Filament

Any modern filament style 3D printer should be able to print these parts as there is not a ton of complexity in them. We personally have utilized a [Prusa i3 MKS+](https://www.prusa3d.com/category/original-prusa-i3-mk3s/) with and without the MMU2s+. We also have a [Prusa XL](https://www.prusa3d.com/product/original-prusa-xl-7/) on order and will add additional 3MF tray layouts when it has arrived and been tested. Please note that we have not tested these parts with Resin style printers.

An entire system can be printed from a standard 1KG roll of filament with plenty to spare. We personally have had success with both PLA and PETG from Hatchbox, but your favorite filament should work fine. Since this is a light sensitive application, we suggest using a black filament. The 3MF PrusaSlicer files provided in this repo are set up for PLA printing but that can easily be changed to PETG.

- [Hatchbox PLA](https://amzn.to/3M1vtyP)
- [Hatchbox PETG](https://amzn.to/3M1vtyP)

## Hardware

This system is assembled using 3mm (M3) flat top hex screws. We have chosen to use Black Oxide Steel from McMaster Carr in both 8mm and 14mm lengths. You will also need a very small number of M3 nuts.

- M3 Black Oxide Screws - 8mm - [Amazon](https://amzn.to/3nRYeWS) | [McMaster-Carr](https://www.mcmaster.com/91294A128/) (14 total required)
- M3 Black Oxide Screws - 14mm - [Amazon](https://amzn.to/3BnAgFP) | [McMaster-Carr](https://www.mcmaster.com/91294A133/) (8 total required)
- M3 Hex Nuts - [Amazon](https://amzn.to/3o0wbEI) | [McMaster-Carr](https://www.mcmaster.com/98676A100/) (6 total required)
- 1/4-20 x 1/2” Screws - 2 (optional)

## Optics

This system has been designed around 2 speciality optics that will need to be sourced, a dicronic beamsplitter and a longpass filter. Other optics will likely work but new internal mounts may need to be designed.

- Beamsplitter - link TBD
- Longpass Filter - link TBD

## Cameras

This system was designed to mount 2x Sony A6400 cameras. Other cameras may work, especially smaller bodies, but the A7 series cameras are too large for this design. Use other camera models at your own risk.

- Sony A6400 - [Amazon](https://amzn.to/3BkoNqC)
- Sony A6400 Full Spectrum Conversion Service - [LifePixel](https://www.lifepixel.com/shop/our-services/full-spectrum-camera-conversion/sony-mirrorless-full-spectrum-conversion/?ar=185&campaign=docs-readme)
- Sony A6400 Full Spectrum Converted Camera - [LifePixel (used)](https://www.lifepixel.com/shop/converted-cameras/used-cameras/used-sony-a6400-camera-conversion/?ar=185&campaign=docs-readme)

## Lenses

Sourcing a lens that not only can transmit UV light but also has consistent focusing across both UV and Visible light is a challenge. In our experiments, we have utilized three different lenses from Nikon’s enlarger series (the Nikon EL-Nikkor Enlarging lenses). Sourcing these lenses can be difficult but we have found eBay to have reasonable availability so far. We have included models for mounting each of these three lenses in the Novoflex Bellows. Currently, the DIY Bellows is designed to only mount the 80mm lens with no plans to create additional options.

- 80mm f5.6
- 135mm f5.6
- 210mm f5.6

## Bellows

We have designed the system where two different bellows can be utilized for focusing. We highly recommend utilizing the commercially available option as it is much more robust and functional. With that caveat, we have chosen to include directions here for the DIY bellows as well.

### Commercial Option

The models provided on this site are specific to the linked Novoflex BALPRO 1 bellows linked below. Other bellows may work but we have found this to be a very robust system capable of supporting a range of lenses.

- Novoflex BALPRO 1 - [Amazon](https://amzn.to/3W3cQPS) | [B&H](https://www.bhphotovideo.com/c/product/269730-REG/Novoflex_BALPRO_1_BALPRO_1_Universal_Bellows.html)

### DIY Option

For the DIY Option, you will need a semi stiff fabric that is light tight. During our testing, we purchased a yard of a thin faux black leather from Joann Fabrics that worked for our purposes. The fabric was cut using a laser and then assembled with contact cement.

## Cable Release

To trigger the cameras simultaneously and reliably, we suggest splicing two shutter release cables into one. Directions for this process can be found below.

- Shutter Release Cables (x2) - [Amazon](https://amzn.to/3M2gWmG)

## Miscellaneous Tools

You will find you may also need need a few tools for the final assembly:

- Hex Key or Wrench that fits the M3 screws (typically a 2mm)
- Needle nose pliers can be handy
- Contact Cement (if building DIY Bellows)

# Printing

We have included all the individual STL files in the [/parts/individual_parts](/parts/individual_parts/) directory if you need to print one off parts or aren’t using a Prusa printer. Even if you aren’t using a Prusa Printer, we recommend you download the free [PrusaSlicer](https://www.prusa3d.com/page/prusaslicer_424/#_ga=2.64626799.1923812417.1683981997-1623575580.1671629586) software and open the 3MF files included in the [/parts](/parts/) directory to reference the recommended part orientation, support positioning, and settings.

For prototyping and testing purposes, all parts were printed utilizing Prusa’s MK3S+ profiles and the `0.15mm Quality` preset for PLA or PETG filaments. Supports are needed for only a few parts and are indicated in the 3MF files where necessary.

## Tray Descriptions

You will find 3 files in the [/parts](/parts/) folder that show the recommended print orientation. If using a Prusa printer or a printer supported in Prusa Slicer, you can simply change your settings and print from there.

### CoreSystem_Tray

This includes all the parts of the core system without the lens facing side of the cube as that is included with the two bellows files as it depends on which you choose to utilize. Essentially, this includes the 5 sides of the cube, the internal optics mounting components, and a support to lock the two cameras together.

### NovoflexBellows_Tray

This includes all the parts required to use the commercially available Novoflex BALPRO1 bellows. Please note that this tray includes all 3 lens adapters (80mm, 135mm, and 210mm). You should only print the adapters for the lenses you will be using.

### DIYBellows_Tray

This includes all the parts required to print and built the DIY Bellows except of course the fabric. Again, we strongly recommend the Novoflex Bellows but if you are determined to use this system, we have included it as an option.

## Individual Part Descriptions

Individual part descriptions can be found on the [Readme in the /parts/individual_parts](/parts/individual_parts/README.md) folder

# System Assembly

## Part 1 - Build the Internal Optics Assembly

We will start by putting together the internal optics assembly that holds all of the internal components. This is the most fiddly of the steps so be patient. Once this is assembled, it gets easier from here.

You will need the following:

### Printed Parts

- Mirror Mount
- Mirror Latch
- Internal Bracket
- Internal Bracket Mirrored
- UV Bandpass Filter Holder
- UV Bandpass Cone
- Visible Cone Baffle
- Internal Shroud

### Required Hardware

- M3 x 8mm Screws - 6
- M3 Nuts - 6

### Optics

- Beamsplitter
- UV Longpass Filter

1. We will start by assembling the UV Filter holder and shrouds. First, we will add the Internal Shroud to the UV Filter Holder. Place the UV Filter Holder on your work surface with the opening towards you and the the countersinks for the two outside holes facing towards the work surface. The Internal Shroud will attach to the bottom of the UV Filter Holder with 2x 8mm M3 screws and 2x M3 Nuts. The screws should be inserted into the shroud and then up through the filter holder. The Shroud should align with the edge and not block the hole cutout for the filter.

2. Take your 2x Internal Brackets and insert 1x M3 nut into each of the cutouts. Depending on the quality of your print, this might take a little bit of force. It is designed to be able to be inserted with finger pressure but be tight enough that the nut is held in place on its own once inserted.

3. Now we reach the most difficult step in the entire process. We are going to create a sandwich here with 3 different parts and sensitive filter all while having to maintain careful alignment. Take your time and be patient. You also may want to wear cotton gloves during this step to prevent touching the filter with your fingers.

   Place the assembled UV Filter Holder and Shroud on your workspace with the shroud down and away from you (it won’t sit flat). Insert the UV filter into the holder taking note of the orientation. In this orientation, light will be passing from the work surface, up through the filter, and into the camera.

   When the filter is inserted, place the UV Bandpass Cone on top, aligning the flat edge with the flat edge of the bottom shroud. This part will not only capture the filter in place but block stray light from entering the camera.

   On each side of this assembly, you will see a screw hole. Focusing on one side at a time, you will want to insert a 8mm M3 screw from below into the UV Filter Holder, through the UV Bandpass Cone, through one of your Internal Brackets and into the captured nut in that bracket.

   Pay close attention to the alignment of the bracket. It should be extending away from the Internal Shroud you assembled in Step 1 and down away from the cone of the UV Bandpass Cone. When viewed from the side, the diagonal holder on the bracket should be slightly insert. The outside surface should not all be flush. Loosely tighten the screw so that everything is held together and then repeat on the other side. The second side should be much easier now that one screw holds everything together.

4. Next we will add the Visible Cone Baffle to the other end of the Internal Brackets. The key thing to keep in mind here around alignment is that the square side of the baffle should face the internal corner of the Internal Brackets. Insert a screw through the shroud and into the captured nut in the bracket to secure everything in place. Repeat on the other side.

5. At this point you should now have two cones (one with a filter) attached at 90 degrees to each other. We will now slide the beamsplitter into the Mirror Holder and then use the Mirror Latch to hold it in place. This can now be slid into the 45° angled brackets of the Internal Bracket. The Latch should be inserted first. This is only a friction fit and will be locked into place further when we insert this into the cube.

6. You have now completed the most difficult and tedious part of the process! Set this assembly aside for a moment and take a minutes break. Next we will assemble the cube and insert this assembly into it.

## Part 2 - Assemble the Cube

Now that the internal optics assembly is complete, we will assemble the cube together around that assembly.

You will need the following:

### Printed Parts:

- Base
- Side 1
- Side 2
- Rear Camera Face
- Top Camera Face

### Required Hardware

- M3 x 8mm Screws - 8
- M3 x 14mm Screws - 4

1. Set the base on your work surface with the line of 5 holes facing down. The small corner inset should be closest to you and on the left side. If looking down on the part, you should see 2 sets of holes on the left and right sides and that indention on the bottom should be in the lower left corner.
2. Take Side 1 (with the text) and place it text side down on the left of your base piece. The curved indentation on the edge should be on the side farthest from you.
3. Now that we have things oriented, take your base and insert a 14mm M3 screw into one of the holes on the left starting from below the base. While ensuring you keep the correct orientation, align Side 1 over the screw and start it threading into the hole.

   The 3D model of the side has threads modeled into it and depending on the quality of your print, you may experience a tight fit with the screw. Even though this isn’t a self tapping screw, it should clean up your threads as you insert it.

   Just get the screw started. You don’t want to fully tighten it down just yet. We want the side to move a bit right now. Insert the second screw into the other hole to connect Side 1 to the base. Tighten both of these up snug but do not over tighten.

4. With Side 1 attached, lay it flat on your work surface (text side down) with the base coming up on your right. Take your Internal Assembly that you created in Step 1 and position it into the side. Note the orientation of the assembly. You want the filter to be parallel with the base, not perpendicular. You may need to shift the mirror holder a bit as it is only held in place with friction at this point. Everything should slot into the extrusions on Side 1 nicely.

5. Now, carefully lay Side 2 on top of this assembly. It may take some careful alignment but when everything is in the right place, it should snap together with a satisfying click. When that happens, connect Side 2 to the base using the 2 remaining M3 x 14mm screws. This should secure everything in place.

6. At this point, verify that the sides of your assembly are straight and perpendicular to each other. If everything has come together properly it should be a rather secure and robust assembly. If something feels off, something probably isn’t quite aligned correctly internally.

7. Now we will add the two lens plates. Place your base back on your work surface with one cone facing up (with the filter) and one cone facing towards your body. With this orientation, you will insert the Top Camera Face into the cube. This one has the two holes on each side near the middle, not in the corners.

Be careful with the alignment here as it can fit in two orientations. Looking down on the system, you want the small dimple indentation on the Top Camera Face to be on the right side of the assembly. Line up the holes and insert 4x of the 8mm M3 screws. Before you tighten any of the screws down all the way, make sure all are started. Once all are inserted, fully insert them and ensure they sit flush with the cube face so they don’t interfere with the camera mount.

8. Rotate the assembly so the last internal conical shroud is facing up and other missing face is down on the work surface. With the base piece towards your body and the lens mount you just inserted in the previous step away from your body, you will insert the other camera mount. This part can only fit in 2 orientations. The correct one has the small dimple on the left side of the assembly in this orientation. Place it in the correct orientation and insert the 4x remaining 8mm M3 screws.

## Part 3 - The Bellows

We highly recommend using the Novoflex BALRPO bellows linked above and these instructions utilize that. If you have chosen to use the DIY Bellows system, we will have a separate set of instructions for that at a future date and will include a link here.

You will need the following:

### Printed Parts:

- Bellows Cube Face
- Bellows Lens Mount for which ever lens you are using

### Required Hardware:

- M3 x 14mm Screws - 4

### Other Equipment:

- Full Spectrum Converted Camera
- Novoflex BALLPRO Bellows
- Lens of Choice

1. This system has been designed to be as tight and compact as possible to allow the most flexibility in your choices of lenses. As a result of that, it is not possible to remove the Full Spectrum (UV) camera from the assembly with the bellows attached. This is a known inconvenience, but is a tradeoff required to use lenses like the 80mm and still have it focus at infinity. Other approaches are being considered.

2. Mount the Full Spectrum or UV camera to the top of the cube. It should lock into place with the spring loaded pin meant to secure a camera and lens.

3. Align the Bellows Cube Face with the last remaining open side of the cube. This will only fit in two positions, rotated 180 degrees apart and both positions work in this situation. Once aligned, insert 4x of the 14mm M3 screws into the 4 holes and tighten them down.

4. The cube can now be inserted into one end of the bellows and the thumb screws tightened down lightly to secure it.

5. For each lens you want to use, you will need to attach the lens to the bellows adapter. Enlarging lenses are designed to mount through a hole and then use a retaining ring to tighten down and lock the lens in place. Since these lenses are most often purchased used, if your lens is missing a retaining ring, we have found that finding a filter step up or step down ring will often match the threads and can be used as a substitute.

   For all but the 210mm lens, most of the lens will need to extend inside the bellows in order to obtain focus at infinity. Simply remove the retaining ring from your lens, insert it through the adapter, and reinstall the retaining ring to secure it in place.

   You can now mount your lens to the other side of the bellows using the thumbscrews on the bellow. Do not over tighten these thumbscrews as it can weaken the layers in the 3D printed parts.

### Part 4 - Final Assembly

We are getting very close now! Just a few things to finalize.

You will need the following:

### Printed Parts:

- Camera Base Connector (optional)

### Required Hardware:

- 1/4-20 x 1/2” Screws - 2 (optional for camera base connector)

### Other Equipment:

- Visible Camera
- Tripod

1. Attach the Visible camera to the last remaining lens mount. Keep in mind that this camera is actually going to end up being mounted upside down with the tripod mounting pointing up. It should lock into place with the spring loaded pin meant to secure a camera and lens.

2. (Optional) For extra rigidity, it is recommended to lock the positioning of the two cameras together using the Camera Base Connector angle bracket. Simply use 2x 1/4-20 screws to connect this bracket to the two cameras and lock them together. This should help with automated post processing as the positioning of the two cameras can’t change relative to each other.

3. When using the Noveoflex BALLPRO bellows, we suggest mounting the bellows to a tripod with the cameras and cube assembly hanging unsupported. We will be developing an additional bracket to help support this connection, but it is preferred over mounting the cube to the tripod and letting the heavier bellows hang off the front.

# Camera Trigger

While it is possible to use a infrared wireless trigger with this set up, we have found that these types of triggers that require line of sight can be a bit unreliable given the orientation of the camera system. They often work, but if you are looking for consistent reliability, we suggest hard wiring a trigger system. To create this system, we simply have purchased two commercially available triggers, cut the cables and spliced them together. You have two options on how you want to splice these wires. If you have the tools and experience, you can solder the splices or you can simply strip the wires and use some mechanical connectors to make the splices.

We have used these [inexpensive cable releases from Amazon](https://amzn.to/3Mnx4PV), but most any wired cable release should work following these principles.

The general approach here is to cut both cable releases approximately 18" from the camera connector. This should give you plenty of working space to connect the cables to both cameras with putting stress on your spliced joint.

After cutting it, you will want to use a wire stripper to strip back the insulation for the bundle and then strip back each of the three component wires exposing a quarter inch or so of bare wire. Do this for both camera connector ends. Repeat this same process on one of the cable release buttons wires as well. You will only need one of the buttons.

Now, we will need to connect these wires together. You should have 3 different covered cables exposed. While your colors may vary from brand to brand, you essentially want to join like colors--the three whites, the three yellows, and the three reds (or whatever colors your bundle happens to have).

## Splicing by Soldering

If you plan to solder, we will do this one wire at a time. Before you start, it is helpful to add a large piece of heat shrink tubing on the button cable that you can shrink down to cover all the joints at the end. If you don't have this, then you can simply wrap the entire joint in electrical tape upon completion.

Working wire by wire, you will want to solder the joints together connecting the single wire from the button side to the two wires of the same color on the two camera connector sides. Again, it is helpful to use a small piece of heat shrink tubing on the button side of each wire to cover, protect, and isolate the joint after soldering. After soldering, heat the heat shrink tubing to cover the joint, and repeat until all joints are soldered. Then position the larger piece of tubing over all the joints and hit it to shrink it down and secure the splices.

## Splicing with Quick Connectors

There are many different types of small connectors on the market that allow you to splice wires like this without soldering. While we personally have not used these at the time of writing, we will be experimenting with them in the future and wanted to include the option here. Follow the same process as with soldering except instead use these small lever activated or heat gun activated connectors. As these are a bulkier option than splicing, you may want to place them inside a small project box. We will include more details around our exact solution in the future.

### Physical Solderless Connector Options

- "3 in 6 out" Splicing Connector - [Amazon](https://amzn.to/3o7lCiZ)
- Lever Action Push in Connectors - [Amazon](https://amzn.to/3pFXKUb)
- Kit of Assorted Push in Connectors - [Amazon](https://amzn.to/41Cpd6i)
- Solder Seal Wire Connectors (needs a heat gun) - [Amazon](https://amzn.to/3nWnjQr)

## A Note on Triggering Video

With this set up we are using a very simple 3 wire trigger system that essentially mimics pressing the shutter button of the camera. There is no digital transmission over this system. If you'd like to use this system to start and stop video, go into your cameras settings and set the cameras to start/stop video when the shutter button is pressed. This means, you can shoot photographs in any of the photography modes and then switch to the video mode on both cameras and utilize the same switch to start/stop recording.
