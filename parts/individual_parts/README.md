# Part List

# Individual Parts List

## Internal Assembly

Mirror Mount - The mounting bracket to hold the beamsplitter. The beamsplitter slides in from the top.

Mirror Latch - The latch that holds the beamsplitter within the Mirror Mount bracket and keeps it from sliding out the top.

Internal Bracket (print 2) - A bracket that holds together all of the internal components together. Note this is the only part that needs to be printed twice.

Internal Shroud - A small bracket blocking stray light from the lens from hitting the UV filter.

UV Bandpass Filter Holder - Bracket to hold the Longpass filter that sits between the beamsplitter and the UV camera.

UV Bandpass Cone - When connected to the UV Bandpass Filter Holder it will act as a lid, holding the filter into the filter holder and also serves as a cone baffle to keep stray light from hitting the UV Camera’s sensor

Visible Cone Baffle - Sits between the beamsplitter and the visible camera and acts as a baffle to block stray light from the visible cameras sensor.

## Cube Assembly

Base - The base face of the cube

Rear Camera Face - The rear face of the cube assembly where the Visible camera is mounted

Side1 - The left side of the cube (contains text)

Side2 - The right side of the cube (no text)

Top Camera Face - The top face of the cube assembly where the UV Camera is mounted

## Bellows Assembly

Bellows Cube Face - The final face of the cube that adapts it to the mounting system of the Novoflex bellows

Bellows Lens Mount-80mm - Adapter to use the Nikkor-EL 80mm lens with the bellows

Bellows Lens Mount-135mm - Adapter to use the Nikkor-EL 135mm lens with the bellows

Bellows Lens Mount-210mm - Adapter to use the Nikkor-EL 210mm lens with the bellows

## Final Assembly

Camera Base Connectors - Right angle bracket to secure the two camera bases together

## DIY (Homemade) Bellows

Homemade Bellows Base Extension

Homemade Bellows Cube Face

Homemade Bellows Internal Cube Side

Homemade Bellows Internal Large

Homemade Bellows Internal Lens Side

Homemade Bellows Rail

Homemade Bellows Rail End Stop

Homemade Bellows Sliding Plate
